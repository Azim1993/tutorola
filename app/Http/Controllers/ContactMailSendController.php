<?php

namespace App\Http\Controllers;

use App\Mail\ContactMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class ContactMailSendController extends Controller
{
    protected function sendContactMail(Request $request)
    {
        $this->validate($request, ['name' => 'required', 'email' => 'required', 'message' => 'required']);
        Mail::to('azimnurul1993@gmail.com')->send(new ContactMail($request->all()));
    }
}
